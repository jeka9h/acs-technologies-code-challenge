﻿const form = document.querySelector('form');

form.addEventListener('submit', event => {
    event.preventDefault();

    let formData = new FormData(form);

    sendRequest(formData);
});
function sendRequest(formData) {
    if (fetch) {
        fetch(form.action, {
            method: 'POST',
            body: formData
        })
            .then(x => {
                alert("Feedback is sent.");
            });
    }
    else {
        let request = new XMLHttpRequest();

        request.open("POST", form.action);

        request.onreadystatechange = function () {
            if (request.readyState == 4 && request.status == 200) {
                alert("Feedback is sent.");
            }
        }
        request.send(formData);
    }
}