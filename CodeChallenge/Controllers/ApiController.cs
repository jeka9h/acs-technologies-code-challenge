﻿using CodeChallenge.Data.Repositories;
using CodeChallenge.Models;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace CodeChallenge.Controllers
{
    [Route("api")]
    [ApiController]
    public class ApiController : ControllerBase
    {
        private readonly FeedbackRepository _feedbackRepository;
        public ApiController()
        {
            _feedbackRepository = new FeedbackRepository();
        }
        [HttpPost("sendfeedback")]
        
        public async Task<IActionResult> SendFeedback([FromForm] Feedback feedback)
        {
            await _feedbackRepository.AddAsync(feedback);
            return Ok();
        }
    }
}
