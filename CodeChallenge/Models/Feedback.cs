﻿using System;

namespace CodeChallenge.Models
{
    public class Feedback
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
    }
}
