﻿using CodeChallenge.Models;
using System.Threading.Tasks;

namespace CodeChallenge.Data.Repositories
{
    public class FeedbackRepository
    {
        private readonly ApplicationContext _appContext;

        public FeedbackRepository()
        {
            _appContext = new ApplicationContext();
        }

        public async Task AddAsync(Feedback feedback)
        {
            await _appContext.AddAsync(feedback);
            await _appContext.SaveChangesAsync();
        }
    }
}
